﻿using Protocol;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Console = System.Console;

namespace Server
{
    public class Server
    {
        private const int MAX_PENDING_CONNECTIONS = 10;

        //Socket ist unsere programmatische Schnittstelle zum Netzwerk.
        //Basis des Sockets ist ein Networkstream der sich grundsätzlich gleich verhält wie jede
        //andere Form von stream(File-/MemoryStream).
        //Ein Socket wird immer auf einen bestimmten Port gebindet auf den er hört.
        private Socket m_serverSocket;

        private bool m_isListening = true;

        private List<ClientServant> Servants
        {
            get
            {
                lock(m_servantLock)
                {
                    return m_servants;
                }
            }
        }
        private readonly object m_servantLock = new object();
        private List<ClientServant> m_servants = new List<ClientServant>();

        public static void Main(string[] args)
        {
            Console.WriteLine("***** Simple Socket Server! *****");
            var server = new Server();
            server.Start();
            server.Run();
            server.Dispose();
        }

        private void Console_CancelKeyPress(object sender, System.ConsoleCancelEventArgs e)
        {
            Console.WriteLine("Exiting...");
            m_isListening = false;
        }

        public void Start()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            Console.WriteLine("Binding socket on " + IPAddress.Any + ":" + Prot.PORT);
            m_serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_serverSocket.Bind(new IPEndPoint(IPAddress.Any, Prot.PORT));
            m_serverSocket.Listen(MAX_PENDING_CONNECTIONS);
            Console.WriteLine("Awaiting client connections...");
        }

        private void Run()
        {
            try
            {
                while (m_isListening)
                {
                    var clientSocket = m_serverSocket.Accept();
                    Console.WriteLine("Client connected from remote end point: " + clientSocket.RemoteEndPoint);
                    var servant = new ClientServant();
                    servant.Start(this, clientSocket);
                    m_servants.Add(servant);
                }
            }
            catch (System.Exception _e)
            {
                Console.WriteLine("Error occurred while receiving client message: " + _e.Message);
            }
        }

        public void OnMessageReceived(string _message)
        {
            Console.WriteLine("Message received from client: " + _message);
        }

        public void OnClientDisconnected(ClientServant _servant)
        {
            Console.WriteLine("Client servant disconnected: " + _servant);
            m_servants.Remove(_servant);
        }

        public void Broadcast(string _message)
        {
            foreach (var servant in Servants)
                servant.Send(_message);
        }

        public void Dispose()
        {
            if(m_serverSocket != null && m_serverSocket.Connected)
                m_serverSocket.Close();
        }
    }
}
