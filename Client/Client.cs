﻿using Protocol;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Console = System.Console;

namespace Client
{
    public class Client
    {
        private Socket m_clientSocket;
        private Thread m_clientListenerThread;

        private bool m_isListening;

        public static void Main(string[] args)
        {
            var client = new Client();
            client.Run();
            client.Dispose();
        }

        private void Run()
        {
            Console.WriteLine("***** Simple Socket Client! *****");
            do
            {
                string host = ReadHostInput();
                IPAddress[] addresses = FetchHostInfo(host);
                IPAddress add = null;
                foreach (var address in addresses)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        add = address;
                        break;
                    }
                }

                if (add == null)
                {
                    Console.WriteLine($"Failed to resolve hostname: {host}");
                }
                else
                {
                    Start(add);
                }
            } while (PromptForContinue());
        }

        private void Send(string _message)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(_message.Trim() + Prot.MSG_DELIMITER);
                m_clientSocket.Send(bytes, 0, bytes.Length, SocketFlags.None);
            }
            catch (System.Exception _e)
            {
                Console.WriteLine("Failed to send message: " + _e.Message);
            }
        }

        private void Start(IPAddress _hostAddress)
        {
            try
            {
                //1. AddressFamily bezeichnet die Art von vermittlungsprotokoll die wir nutzen möchten.
                //2. Socket type beschreibt die zugrundeliegende socket-art.
                //3. Protokolltyp entweder UDP oder TCP je nachdem was gebraucht wird.
                m_clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //Ip end point ist nichts weiteres als eine wrapper klasse die IP-addresse und Port zusammenfasst.
                IPEndPoint endpoint = new IPEndPoint(_hostAddress, Prot.PORT);
                m_clientSocket.Connect(endpoint);
                if (m_clientSocket.Connected)
                {
                    Console.WriteLine("Client successfully connected to " + endpoint);
                    m_clientListenerThread = new Thread(RunListener);
                    m_clientListenerThread.Start();
                    Send("foobar!");
                }
            }
            catch (System.Exception _e)
            {
                Console.WriteLine($"Client failed to connect({_e.GetType().Name}): {_e.Message}");
            }
        }

        private void RunListener()
        {
            byte[] data = new byte[Prot.PACKET_SIZE];
            string message = "";
            try
            {
                while (m_clientSocket.Receive(data, 0, data.Length, SocketFlags.None) > 0)
                {
                    message += Encoding.UTF8.GetString(data).Replace("\0", "");

                    if (message.Contains(Prot.MSG_DELIMITER))
                    {
                        OnMessageReceived(message.Replace(Prot.MSG_DELIMITER, ""));
                        message = "";
                    }
                }
            }
            catch { }
        }

        private void Dispose()
        {
            if (m_clientSocket != null && m_clientSocket.Connected)
                m_clientSocket.Close();
        }

        private string ReadHostInput()
        {
            var hostInput = "";
            while (string.IsNullOrWhiteSpace(hostInput))
            {
                Console.WriteLine();
                Console.Write("Enter hostname: ");
                hostInput = Console.ReadLine();
            }
            return hostInput;
        }

        private bool PromptForContinue()
        {
            System.ConsoleKeyInfo value = new System.ConsoleKeyInfo();
            while (value.Key != System.ConsoleKey.N &&
                    value.Key != System.ConsoleKey.Y)
            {
                Console.WriteLine();
                Console.Write("Do you want to continue?(y/n): ");
                value = Console.ReadKey();
            }
            return (value.Key == System.ConsoleKey.Y);
        }

        private IPAddress[] FetchHostInfo(string _hostName)
        {
            try
            {

                //hier passiert die magie!
                //der hostname wird an den zuständigen DNS server übermittelt,
                //wenn der host gefunden wurde gibt der DNS server die jeweiligen IP-Addressen
                //zurück.
                //Aufpassen, denn hier wird Netzwerkcode ausgeführt! 
                return Dns.GetHostAddresses(_hostName);
            }
            catch
            {
                return null;
            }
        }

        private void OnMessageReceived(string _message)
        {
            Console.WriteLine("Server message received: " + _message);
        }
    }
}
