﻿using Protocol;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Console = System.Console;

namespace Server
{
    public class ClientServant
    {
        private Socket m_clientSocket;

        private Thread m_clientThread;

        private Server m_server;

        public void Start(Server _server, Socket _clientSocket)
        {
            m_server = _server;
            m_clientSocket = _clientSocket;
            Console.WriteLine("Starting client servant for remote end point: " + this);
            m_clientThread = new Thread(Run);
            m_clientThread.Start();
        }

        public void Dispose()
        {
            m_server.OnClientDisconnected(this);
            if(m_clientSocket != null && m_clientSocket.Connected)
                m_clientSocket.Close();
        }

        public void Send(string _message)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(_message.Trim() + Prot.MSG_DELIMITER);
                m_clientSocket.Send(data, 0, data.Length, SocketFlags.None);
            }
            catch(System.Exception _e)
            {
                Console.WriteLine("Error occurred while sending message: " + _e.Message);
            }
        }

        private void Run()
        {
            byte[] data = new byte[Prot.PACKET_SIZE];
            string message = "";
            try
            {
                while (m_clientSocket.Receive(data, 0, data.Length, SocketFlags.None) > 0)
                {
                    message += Encoding.UTF8.GetString(data).Replace("\0", "");

                    if (message.Contains(Prot.MSG_DELIMITER))
                    {
                        m_server.OnMessageReceived(message.Replace(Prot.MSG_DELIMITER, ""));
                        message = "";
                    }
                }
            } catch { }

            Dispose();
        }

        public override string ToString()
        {
            return m_clientSocket?.RemoteEndPoint?.ToString() ?? "unknown";
        }
    }
}
