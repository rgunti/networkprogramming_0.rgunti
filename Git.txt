Source Tree Atlassian

Repository
	Ist das gesamte Projekt, FileSystem + Abstraktes Git File System mit allen Branches, Tags etc.
	Zu diesem Repository z�hlen alle File Systeme auf allen beteiligten Ger�ten (server, clients)

Working Copy
	Aktuelles, tats�chlich vorhandenes File system. 

Clone
	Initialisiert das lokale Repository und l�d alle enthaltenen dateien
	herunter.
	
Commit
	Ein commit wendet die aktuellen �nderungen am lokalen File system auf seine abstrakte Representation an.
	Der commit bedeutet aber im gegensatz zu z.B. dem commit in SVN nicht dass die �nderungen direkt an
	den server weitergegeben werden.	
	Commits sind einzelne Zust�nde unseres Projektes �ber die Zeit.

Checkout
	Wechselt das aktuelle abstrakte Dateisystem (Branch) in dem wir arbeiten wollen.
	Alle commits werden in den aktiven Branch �bernommen.
	Um den Branch zu wechseln m�ssen alle lokalen �nderungen am Dateisystem erst Committet werden.

Push	

	�bernimmt dann den Zustand des aktuellen Branches in unser Remote Repository.

Pull
	
	Holt aktuellen Zustand des Remote Repository in den aktuellen lokalen Branch.
	
Branch

	Ein branch ist eine abstrakte Version unseres Projektes im git repository.

Tags
	
	Tags sind named commits. Im Normalfall erstellt man f�r eine erreichte versionsnummer einen Tag.
	So ist sch�n nachvollziehbar wie sich das Projekt entwickelt und wir k�nnen jederzeit zu einer 
	spezifischen Version springen.
	
Merge

	Mithilfe des merges k�nnen k�nnen die �nderungen eines Branches in einen anderen �bernommen werden.
	Es wird immer ein inaktiver branch in den aktiven branch (per checkout festgelegt) gemergt.
	Der Aktive branch ist "Mine"(Local), der zu mergende Branch ist "Theirs"(Remote).

Fetch

	Aktualisiert die Darstellung im git client so dass die �nderungen die auf dem server liegen dargestellt werden.

Stash
	
	Gibt uns die M�glichkeit �nderungen am lokalen File system vorl�ufig auszulagern um keine lokalen �nderungen
	mehr zu haben. Wenn wir z.B. den Branch wechseln wollen ohne unsere lokalen �nderungen zu committen k�nnen wir 
	die �nderungen erst stashen, dann den Branch wechseln und wenn wir die �nderungen wieder haben wollen den 
	Stash auf unseren lokalen Branch applien.

Fork

	Wir "spalten" das Repository auf und erstellen ein neues Repository auf basis des geforkten Repositories.
	Die beiden repositories laufen ab diesem Zeitpunkt unabh�ngig voneinander.
	
Pull request

	Nachdem ein repository geforkt wurde und die �nderungen in das original repository �bernommen werden sollen,
	wird ein Pull request an den inhaber des original repositories gestellt. Der pull request ist die Anfrage ob
	die �nderungen im geforkten Repository in das original repository �bernommen werden sollen.
	
	
	
	
	
	
	
	
	
	
	
	