﻿namespace Protocol
{
    public class Prot
    {
        public const int PORT = 25565;

        public const int PACKET_SIZE = 2048;

        public const string MSG_DELIMITER = "%";
    }
}
