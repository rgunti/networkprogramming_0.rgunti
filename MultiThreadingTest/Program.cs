﻿using System.Collections.Generic;
using System.Threading;

namespace MultiThreadingTest
{
    public class Program
    {
        private Thread[] m_threads;

        private List<string> Values
        {
            get
            {
                //Blockt den zugriff auf den enthaltenen code solange bis 
                //er nicht mehr von einem anderen thread verwendet wird.
                //So kann verhindert werden dass verschiedene threads zur selben 
                //zeit auf den gleichen speicher zugreifen.
                lock (m_lockObject)
                {
                    return m_values;
                }
            }
        }
        private List<string> m_values = new List<string>();

        private int m_counter;

        private readonly object m_lockObject = new object();

        public Program()
        {
            m_threads = new[] 
            {
                new Thread(Run) { Name = "thread_1" },
                new Thread(Run) { Name = "thread_2" },
                new Thread(Run) { Name = "thread_3" },
                new Thread(Run) { Name = "thread_4" },
                new Thread(Run) { Name = "thread_5" }
            };

            foreach (var thread in m_threads)
                thread.Start();

            while(m_counter != 5)
            {
                Thread.Sleep(100);
            }

            System.Console.WriteLine("- " + string.Join("\n- ", m_values));
            System.Console.ReadKey();
        }

        private void Run()
        {
            for(int i = 0; i < 10; ++i)
            {
                Values.Add(Thread.CurrentThread.Name + ": " + i);
            }
            ++m_counter;
        }

        public static void Main(string[] args)
        {
            new Program();
        }
    }
}
